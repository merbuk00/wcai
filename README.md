# README #
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)  

### Writing Compiler and Interpreters by *Ronald Mak* rewrite ###
This is the rewrite of **Writing Compiler and Interpreters**, *2nd Edition* by *Ronald Mak* into C++ for Code::Blocks + MingW.    

*Status*  
This is the root folder with sub-foders mapping on to the book chapters.    

### How do I get set up? ###

**Summary of set up**  
Development environment is [Code::Blocks](http://www.codeblocks.org). Download the Windows binary release that comes with MingW (MingW stands for Minimalist GNU for Windows). In reality, MingW is the compiler and Code::Blocks is just an IDE (Integrated Development Environment) that you can use with different compilers. If you download the version that does not come with MingW, you will have to install MingW manually. To avoid complications, just make sure you get the version that comes packaged with MingW.    

**Setting Up SDL**  
Setting up SDL is not necessary, but I have it here as my development environment is already set up for this. Download the [SDL libraries](https://www.libsdl.org). You will need the *SDL 1.2 Development* library, that are configured for MingW. Ensure the the correct version (32 or 64-bit is used). Once downloaded th, create a folder on the C drive of your computer (or any drive that is convenient) and name it *SDL*.    

Download extensions of SDL: [images](http://www.libsdl.org/projects/SDL_image/release-1.2.html), [sound](http://www.libsdl.org/projects/SDL_mixer/release-1.2.html), and [font ](http://www.libsdl.org/projects/SDL_ttf/release-1.2.html). The names of the files are **SDL_image-devel-1.2.12-VC.zip**,
**SDL_mixer-devel-1.2.12-VC.zip**, and **SDL_ttf-devel-2.0.11-VC.zip**.    

After extracting each of those files, they should each contain a folder called *include* and a folder called *lib*. Copy the files in the lib folder that have a *.lib* extension and paste them in the *lib* folder in your SDL directory. Next, copy the files in the *include* directory and paste them in the folder called *SDL*, which is located in the include folder of your *SDL* directory. There are also DLL files in the lib folder, and these are going to be necessary later on, so do not lose them.    

**Configuring Code::Blocks**  
Open Code::Blocks and click on *Settings → Compiler and Debugger*. Then, click on *Global Compiler Settings → Linker Settings*. In the *Other Linker Options* box, type **-lmingw32 -lSDLmain -lSDL -lSDL_image -lSDL_ttf -lSDL_mixer**. This list doesn’t have to be on the same line; each item can be on a separate line. After that, go to the *Search Directories* tab. Under the *Compiler* tab, click on *Add* and add the SDL include directory. You can also use the *Browse* button to locate the folder. Now go to the *Linker* tab and add *lib* folder location.    

**Dependencies**  
*libfreetype-6.dll*  
*libogg-0.dll*  
*libvorbis-0.dll*  
*libvorbisfile-3.dll*  
*mikmod.dll*  
*SDL.dll*  
*SDL_image.dll*  
*SDL_mixer.dll*  
*SDL_ttf.dll*  
*smpeg.dll*  
*zlib1.dll*  
