//fig 2-3
//  *************************************************************
//  *                                                           *
//  *   I / O   B U F F E R S   (Header)                        *
//  *                                                           *
//  *   CLASSES: TTextInBuffer,  TSourceBuffer                  *
//  *            TTextOutBuffer, TListBuffer                    *
//  *                                                           *
//  *   FILE:    prog2-1/buffer.h                               *
//  *                                                           *
//  *   MODULE:  Buffer                                         *
//  *                                                           *
//  *   Copyright (c) 1996 by Ronald Mak                        *
//  *   For instructional purposes only.  No warranties.        *
//  *                                                           *
//  *   Modified by K Pillai for readability (Oct 2017)         *
//  *                                                           *
//  *************************************************************

#ifndef buffer_h
#define buffer_h

#include <fstream>
#include <stdio.h>
#include <string.h>
#include "misc.h"
#include "error.h"

//              ***********
//              *         *
//              *  Input  *
//              *         *
//              ***********

extern char eofChar;
extern int  inputPosition;
extern int  listFlag;
extern int  level;

const int maxInputBufferSize = 256;

//--------------------------------------------------------------
//  TTextInBuffer       Abstract text input buffer class.
//--------------------------------------------------------------

class TTextInBuffer
{
protected:
    std::fstream  file;                 // input text file
    char    *const pFileName;           // ptr to the file name
    char     text[maxInputBufferSize];  // input text buffer
    char    *pChar;            // ptr to the current char
                                        //   in the text buffer
    virtual char GetLine(void) = 0;

public:
    TTextInBuffer(const char *pInputFileName, TAbortCode ac);
    virtual ~TTextInBuffer(void);

    char Char       (void) const { return *pChar; }
    char GetChar    (void);
    char PutBackChar(void);
};

//--------------------------------------------------------------
//  TSourceBuffer       Source buffer subclass of TTextInBuffer.
//--------------------------------------------------------------

class TSourceBuffer : public TTextInBuffer
{
    virtual char GetLine(void);

public:
    TSourceBuffer(const char *pSourceFileName);
};

//              ************
//              *  Output  *
//              ************

//--------------------------------------------------------------
//  TTextOutBuffer      Abstract text output buffer class.
//--------------------------------------------------------------

class TTextOutBuffer
{
public:
    TTextOutBuffer();
    ~TTextOutBuffer();
    char text[maxInputBufferSize + 16];  // output text buffer

    virtual void PutLine(void) = 0;
    void PutLine(const char *pText);
};

//--------------------------------------------------------------
//  TListBuffer         List buffer subclass of TTextOutBuffer.
//--------------------------------------------------------------

class TListBuffer : public TTextOutBuffer
{
    char *pSourceFileName;  // ptr to source file name (for page header)
    char  date[26];         // date string for page header
    int   pageNumber;       // current page number
    int   lineCount;        // count of lines in the current page

    void PrintPageHeader(void);

public:
    TListBuffer();
    virtual ~TListBuffer(void);

    void Initialize(const char *fileName);

    virtual void PutLine(void);
    void PutLine(const char *pText);
    void PutLine(const char *pText, int lineNumber, int nestingLevel);
};

extern TListBuffer list;

#endif
//endfig

