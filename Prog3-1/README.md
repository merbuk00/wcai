# README #
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)  

### Writing Compiler and Interpreters by *Ronald Mak* rewrite ###
This is the rewrite of **Writing Compiler and Interpreters**, *2nd Edition* by *Ronald Mak* into C++ for Code::Blocks + MingW.    

*Status*  
This is *Program 3-1* in *Chapter 3* of the book.     
